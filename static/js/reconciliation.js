/***************************************************
 ** @Desc : This file for 对账js
 ** @Time : 2019.04.18 10:41
 ** @Author : Joker
 ** @File : reconciliation
 ** @Last Modified by : Joker
 ** @Last Modified time: 2019.04.18 10:41
 ** @Software: GoLand
 ****************************************************/

let recon = {
    do_paging: function () {
        let uName = $("#uName").val();
        let datetimepicker1 = $("#datetimepicker1").val();
        let datetimepicker2 = $("#datetimepicker2").val();
        let u_Status = $("#uStatus").val();
        $.ajax({ //去后台查询第一页数据
            type: "GET",
            url: "/user/reconcile_list/",
            data: {
                page: '1',
                limit: "15",
                uName: uName,
                uStart: datetimepicker1,
                uEnd: datetimepicker2,
                uStatus: u_Status,
            }, //参数：当前页为1
            success: function (data) {
                if (data.root != null) {
                    recon.show_user_data(data.root);//处理第一页的数据

                    let options = {//根据后台返回的分页相关信息，设置插件参数
                        bootstrapMajorVersion: 3, //
                        currentPage: data.page, //当前页数
                        totalPages: data.totalPage, //总页数
                        numberOfPages: data.limit,//每页记录数
                        itemTexts: function (type, page) {//设置分页按钮显示字体样式
                            switch (type) {
                                case "first":
                                    return "首页";
                                case "prev":
                                    return "上一页";
                                case "next":
                                    return "下一页";
                                case "last":
                                    return "末页";
                                case "page":
                                    return page;
                            }
                        },
                        onPageClicked: function (event, originalEvent, type, page) {//分页按钮点击事件
                            $.ajax({//根据page去后台加载数据
                                url: "/user/reconcile_list/",
                                type: "GET",
                                data: {
                                    page: page,
                                    uName: uName,
                                    uStart: datetimepicker1,
                                    uEnd: datetimepicker2,
                                    uStatus: u_Status,
                                },
                                success: function (data) {
                                    recon.show_user_data(data.root);//处理数据
                                }
                            });
                        }
                    };
                    $('#xf_page').bootstrapPaginator(options);//设置分页
                }
            },
            error: function (XMLHttpRequest) {
                toastr.info('something is wrong, code: ' + XMLHttpRequest.status);
            }
        });
    },
    show_user_data: function (list) {
        let con = "";
        $.each(list, function (index, item) {
            let uType = "普通用户";
            switch (item.Authority) {
                case 100:
                    uType = "管理员";
                    break;
                case 101:
                    uType = "VIP用户";
                    break;
                case 102:
                    uType = "普通用户";
                    break;
                case 103:
                    uType = "对接用户";
                    break;
            }

            con += "<tr><td><a>" + (index + 1) + "</a></td>";
            con += "<td>" + item.UserName + "</td>";
            con += "<td>" + item.TotalAmount.toFixed(2) + "</td>";
            con += "<td>" + item.UsableAmount.toFixed(2) + "</td>";
            con += "<td>" + item.FrozenAmount.toFixed(2) + "</td>";

            con += "<td>" + item.PayByRecord.toFixed(2) + "</td>";
            con += "<td>" + item.RechargeByRecord.toFixed(2) + "</td>";
            con += "<td>" + item.TransferByRecord.toFixed(2) + "</td>";
            con += "<td>" + item.ProfitByRecord.toFixed(2) + "</td>";

            con += "<td>" + item.HandleAddition.toFixed(2) + "</td>";
            con += "<td>" + item.HandleDeduction.toFixed(2) + "</td>";

            con += "<td>" + item.RechargeByRecordToday.toFixed(2) + "</td>";
            con += "<td>" + item.PayByRecordToday.toFixed(2) + "</td>";
            con += "<td>" + item.TransferByRecordToday.toFixed(2) + "</td>";
            con += "<td>" + item.ProfitByRecordToday.toFixed(2) + "</td>";

            con += "<td>" + item.BalanceByRecord.toFixed(2) + "</td>";

            con += "<td>" + uType + "</td>";
            con += "</td></tr>";
        });
        $("#your_showtime").html(con);
    },
};